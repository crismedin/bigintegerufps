/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.BigInteger_UFPS;
import java.math.BigInteger;

/**
 *
 * @author madar
 */
public class BigFactorial_UFPS {

    private BigInteger_UFPS numero;
    
    public BigFactorial_UFPS() {
    }
    

    /**
     *  Constructor que recibe el número a ser tratado el proceso de factorial
     * @param numero un entero positivo
     * @throws Exception genera excepción si el numero es negativo
     */
    public BigFactorial_UFPS(String numero) throws Exception{
        
        validar(numero);
        BigInteger_UFPS n=new BigInteger_UFPS(numero);
        this.numero = n;
    }
    
    private void validar(String numero)throws Exception{
        for (int i = 0; i < numero.length(); i++) {
            if(!esNumero(numero.charAt(i)))
                throw new Exception("El numero no es valido");
        }
        BigInteger_UFPS n=new BigInteger_UFPS(numero);
        if (n.toString().equals(BigInteger_UFPS.ZERO.toString())) 
            throw new Exception("No se puede calcular el factorial de 0 o negativo");
    }
    
    private boolean esNumero(char a){
        return a >= 49 && a <= 57 || a == '0';
    }
    
    /**
     * Obtiene el número factorial del atributo numero
     * @return un entero con el número factorial
     */
    public BigInteger_UFPS getFactorial() 
    {   
        BigInteger_UFPS fac=new BigInteger_UFPS("1");
        for(int i=1;i<=this.numero.intValue();i++)
        {
            BigInteger_UFPS i_big=new BigInteger_UFPS(i+""); //23456
            fac=fac.multiply(i_big);
        }
        return fac;
    }

    @Override
    public String toString() {
        return this.numero.toString();
    }
    
    
    
    
    
    
    
    
}
